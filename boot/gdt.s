GDT_START:
  ;null segment
  dq 0
GDT_CODE:
  ;code segment double word 1
  dw 0xffff                     ;limiter (4GB)
  dw 0                          ;base addr
  ;code segment double word 2
  db 0                          ;base addr (bits 16-23)
  db 0b10011010                 ;type (8-12), priv level (13-14), present flag (15)
  db 0b11001111                 ;limit (16-19), attrs (20-22), granularity (23)
  db 0                          ;base addr (bits 24-31)
GDT_DATA:
  ;data segment double word 1
  dw 0xffff                     ;limiter (4GB)
  dw 0                          ;base addr
  ;data segment double word 2
  db 0                          ;base addr (bits 16-23)
  db 0b10010010                 ;type (8-12), priv level (13-14), present flag (15)
  db 0b11001111                 ;limit (16-19), attrs (20-22), granularity (23)
  db 0                          ;base addr (bits 24-31)
GDT_END:

GDT_DESC:
  dw GDT_END - GDT_START - 1    ;gdt size
  dd GDT_START                  ;gdt addr

  CODESEG equ GDT_CODE - GDT_START
  DATASEG equ GDT_DATA - GDT_START
