puts:
  pusha

  mov ah, 0x0e                  ;tty output
.loop:
  lodsb                         ;al = str[si++]
  or al, al
  je .end
  int 0x10
  jmp .loop
.end:
  popa
  ret


putnl:
  pusha
  mov ax, 0x0e0a                ;nl
  int 0x10
  mov al, 0x0d                  ;cr
  int 0x10
  popa
  ret


putx:                           ;puts hexadecimal of byte in dx
  pusha

  mov cx, 0
.loop:
  cmp cx, 4
  je .end

  mov ax, dx
  and ax, 0xf                   ;take last nibble
  add al, 0x30                  ;'0' + al
  cmp al, 0x39
  jle .next                     ;if al > '9'
  add al, 0x27                  ;('a' - '9') + al
.next:
  mov bx, _PUTX_BUF + 3         ;end of output
  sub bx, cx                    ;move into output based on which char we're on
  mov [bx], al                  ;put char in buffer
  ror dx, 4                     ;rotate to next char

  inc cx
  jmp .loop

.end:
  mov si, _PUTX_BUF
  call puts

  popa
  ret


_PUTX_BUF:
  db '0000', 0
