#ifndef VIDEO_H
#define VIDEO_H

#include <stddef.h>
#include <stdint.h>

extern const size_t VGA_TEXT_BUF_ADDR;

extern const uint8_t VGA_TEXT_BUF_W;
extern const uint8_t VGA_TEXT_BUF_H;
extern const uint16_t VGA_TEXT_BUF_MAX;

extern const uint16_t CRTC_ADDR_PORT;
extern const uint16_t CRTC_DATA_PORT;

extern const uint8_t CRTC_CURSOR_LOC_HIGH_INDEX;
extern const uint8_t CRTC_CURSOR_LOC_LOW_INDEX;

void setcur(uint16_t cur);
uint16_t getcur(void);

void cls(uint8_t colour);
void newline(void);
void advance(void);
void putc(char c, uint8_t colour);
void puts(const char *str, uint8_t colour);
void putx_n(uint8_t nibble, uint8_t colour);
void putx_b(uint8_t byte, uint8_t colour);

#endif
