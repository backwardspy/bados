global in_b
global out_b
global in_w
global out_w

in_b:
  mov dx, [esp+4]
  in al, dx
  ret

out_b:
  mov dx, [esp+4]
  mov ax, [esp+8]
  out dx, al
  ret

in_w:
  mov ax, [esp+4]
  in ax, dx
  ret

out_w:
  mov ax, [esp+4]
  mov dx, [esp+8]
  out dx, ax
  ret
