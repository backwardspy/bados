#include "video.h"
#include "ports.h"

const size_t VGA_TEXT_BUF_ADDR = 0xb8000;
volatile uint16_t * const VGA_TEXT_BUF = (uint16_t * const)VGA_TEXT_BUF_ADDR;

const uint8_t VGA_TEXT_BUF_W = 80;
const uint8_t VGA_TEXT_BUF_H = 25;
const uint16_t VGA_TEXT_BUF_MAX = (uint16_t)VGA_TEXT_BUF_W * (uint16_t)VGA_TEXT_BUF_H;

const uint16_t CRTC_ADDR_PORT = 0x3d4;
const uint16_t CRTC_DATA_PORT = 0x3d5;

const uint8_t CRTC_CURSOR_LOC_HIGH_INDEX = 0xe;
const uint8_t CRTC_CURSOR_LOC_LOW_INDEX = 0xf;

void setcur(uint16_t cur) {
  out_b(CRTC_ADDR_PORT, CRTC_CURSOR_LOC_HIGH_INDEX);
  out_b(CRTC_DATA_PORT, (uint8_t)(cur >> 8));
  out_b(CRTC_ADDR_PORT, CRTC_CURSOR_LOC_LOW_INDEX);
  out_b(CRTC_DATA_PORT, (uint8_t)cur);
}

uint16_t getcur(void) {
  out_b(CRTC_ADDR_PORT, CRTC_CURSOR_LOC_HIGH_INDEX);
  uint16_t cur = in_b(CRTC_DATA_PORT) << 8;
  out_b(CRTC_ADDR_PORT, CRTC_CURSOR_LOC_LOW_INDEX);
  cur |= in_b(CRTC_DATA_PORT);
  return cur;
}

void cls(uint8_t colour) {
  for (int i = 0; i < VGA_TEXT_BUF_MAX; i++) {
    VGA_TEXT_BUF[i] = (uint16_t)colour << 8;
  }
}

void newline(void) {
  uint16_t cur = getcur();
  setcur(cur + VGA_TEXT_BUF_W - (cur % VGA_TEXT_BUF_W));
}

void advance(void) {
  uint16_t cur = getcur();
  setcur(cur + 1);
}

void putc(char c, uint8_t colour) {
  VGA_TEXT_BUF[getcur()] = (uint16_t)colour << 8 | c;
  advance();
}

void puts(const char *str, uint8_t colour) {
  uint16_t i = 0;
  while (str[i]) {
    if (str[i] == '\n') {
      newline();
    } else {
      putc(str[i], colour);
    }
    i++;
  }
}

void putx_n(uint8_t nibble, uint8_t colour) {
  nibble &= 0xf;
  uint8_t c = '0' + nibble;
  if (c > '9') {
    c += ('a' - '9' - 1);
  }
  putc(c, colour);
}

void putx_b(uint8_t byte, uint8_t colour) {
  char chars[3];
  chars[2] = 0;
  for (int i = 0; i < 2; i++) {
    uint8_t nibble = (byte >> (i << 2)) & 0xf;
    uint8_t c = '0' + nibble;
    if (c > '9') {
      c += ('a' - '9' - 1);
    }
    chars[1-i] = c;
  }
  puts(chars, colour);
}
