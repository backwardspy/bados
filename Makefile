osimg = bados.img

cflags = -target i386-elf -ffreestanding -Wall -Werror
c = clang $(cflags)

ldflags = -Ttext 0x1000 --entry kentry
ld = ld.lld $(ldflags)

qemuflags = -drive file=$(osimg),if=floppy,index=0,format=raw
qemu = qemu-system-i386 $(qemuflags)

.PHONY: all debug clean run debug tags

all: $(osimg) tags

$(osimg): build/boot.bin build/kernel.bin
	cat $^ > $@

build/boot.bin: boot/boot.s boot/diskload.s boot/gdt.s boot/pm.s boot/puts.s
	nasm -f bin -o $@ $<

build/kernel.bin: build/kernel_entry.o build/kernel.o build/ports.o build/video.o
	$(ld) -o build/kernel.elf $^
	objcopy -O binary build/kernel.elf $@

build/kernel.o: kernel/kernel.c
	$(c) -o $@ -c $<

build/kernel-dbg.o: kernel/kernel.c
	$(c) -g3 -o $@ -c $<

build/ports.o: drivers/ports.s drivers/ports.h
	nasm -f elf -o $@ $<

build/ports-dbg.o: drivers/ports.s drivers/ports.h
	nasm -f elf -o $@ $<

build/video.o: drivers/video.c drivers/video.h
	$(c) -o $@ -c $<

build/video-dbg.o: drivers/video.c drivers/video.h
	$(c) -g3 -o $@ -c $<

build/kernel-dbg.elf: build/kernel_entry.o build/kernel-dbg.o build/ports-dbg.o build/video-dbg.o
	$(ld) -o $@ $^

build/kernel_entry.o: kernel/entry.s
	nasm -f elf -o $@ $<

clean:
	rm -f $(osimg) build/*

run: $(osimg)
	$(qemu)

debug: $(osimg) build/kernel-dbg.elf
	@echo -e '\n use "gdb -tui -x debug.gdb" to connect to qemu and jump into the kernel code\n'
	$(qemu) -S -s

tags:
	find -type f -name "*.[ch]" -print0 | xargs -0 etags -o TAGS -a -l c
